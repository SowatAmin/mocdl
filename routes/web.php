<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
/*Route::get('/dashboard',function(){
	return view('admin.layout');
});*/
Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/customdata', 'HomeController@customdata');
Route::get('/custom/pdf/{from}/{to}','HomeController@customPdf');

Route::get('/logout', function()
    {
	Auth::logout();
    Session::flush();
    return Redirect::to('/');
    });




