<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usage extends Model
{
    protected $fillable = array('current',
    							 'voltage',
    							 'power',
    							 'time',
    							 'motor_id',
    							 'date',
    							 'status',
    							 'datetime'
    							);

    protected $table='usage_motor';
    

}
