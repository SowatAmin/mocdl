<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Charts;
use App\Usage;
use Auth;
use PDF;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $motor_id = Auth::user()->motor_id;
            
            

           
            
            $usages=Usage::where('motor_id',$motor_id)
                        ->orderBy('created_at', 'desc')
                        ->get();
            
            $current_dateTime = Carbon::today();
            $current_date = $current_dateTime->toDateString();           
             //daily chart starts here
                    
            $daily_data = Usage::where('motor_id',$motor_id)
            ->where('date','=',$current_date)
            ->get();
            $daily_chart = Charts::multi('line', 'highcharts')
            // Setup the chart settings
            ->title("Power,Voltage & Current Graph")
            // A dimension of 0 means it will take 100% of the space
            ->dimensions(0, 400)
            ->template("material")
            // Setup the diferent datasets (this is a multi chart)
            ->dataset('Current', $daily_data->pluck('current'))
            ->dataset('Voltage', $daily_data->pluck('voltage'))
            ->dataset('Power', $daily_data->pluck('power'))            
            // Setup what the values mean
            ->labels($daily_data->pluck('datetime'));
            //monthly charts ends here



            // daily chart ends here

            //monthly chart 
                    
                    //$data = Usage::where('motor_id',$motor_id)->get();
                    
                    
                    $dateTimeSubtracted = $current_dateTime->subDays(30);
                    $dateSubtracted = $dateTimeSubtracted->toDateString();
                    $data = Usage::where('motor_id',$motor_id)
                    ->where('date','<=',$current_date)
                    ->where('date','>=', $dateSubtracted)
                    ->get();

                    $monthly_voltage=$data->pluck('voltage');
                    $monthly_current=$data->pluck('current');
                    $monthly_power=$data->pluck('power');
                    $monthly_datetime=$data->pluck('datetime');

                    /*while (strtotime($dateSubtracted) <= strtotime($current_date))
                         {
                            array_push($dates,$dateSubtracted );
                            $dateSubtracted = date ("Y-m-d", strtotime("+1 day", strtotime($dateSubtracted)));
                
                         }*/
                    /*foreach ($data as $key) {
                        $temp = $key->date ." ". $key->time;
                        array_push($dates,$temp );

                    }
*/
            
            $chart = Charts::multi('line', 'highcharts')
            // Setup the chart settings
            ->title("Power,Voltage & Current Graph")
            // A dimension of 0 means it will take 100% of the space
            ->dimensions(0, 400)
             // Width x Height
           
            ->template("material")
            
            // ->colors(['#2196F3', '#F44336', '#FFC107'])
            // Setup the diferent datasets (this is a multi chart)
            ->dataset('Current', $data->pluck('current'))
            ->dataset('Voltage', $data->pluck('voltage'))
            ->dataset('Power', $data->pluck('power'))            
            ->labels($data->pluck('datetime'));
            
            
            //monthly charts ends here
            

            //weekly charts starts here
            
                   
                    
                    
                    $weekly_dateTimeSubtracted = $current_dateTime->subDays(7);
                    $weekly_dateSubtracted = $weekly_dateTimeSubtracted->toDateString();
                    $weekly_data = Usage::where('motor_id',$motor_id)
                    ->where('date','<=',$current_date)
                    ->where('date','>=', $weekly_dateSubtracted)
                    ->get();

                    


            $weekly_chart = Charts::multi('line', 'highcharts')
            // Setup the chart settings
            ->title("Power,Voltage & Current Graph")
            // A dimension of 0 means it will take 100% of the space
            ->dimensions(0, 400)
             // Width x Height
           
            ->template("material")
            ->dataset('Current', $weekly_data->pluck('current'))
            ->dataset('Voltage', $weekly_data->pluck('voltage'))
            ->dataset('Power', $weekly_data->pluck('power'))            
            ->labels($data->pluck('datetime'));





            //weekly charts ends



            //Yearly chart starts here
            
                    $yearly_dateTimeSubtracted = $current_dateTime->subDays(365);
                    $yearly_dateSubtracted = $weekly_dateTimeSubtracted->toDateString();
                    $yearly_data = Usage::where('motor_id',$motor_id)
                    ->where('date','<=',$current_date)
                    ->where('date','>=', $yearly_dateSubtracted)
                    ->get();

                    


            $yearly_chart = Charts::multi('line', 'highcharts')
            // Setup the chart settings
            ->title("Power,Voltage & Current Graph")
            // A dimension of 0 means it will take 100% of the space
            ->dimensions(0, 400)
             // Width x Height
           
            ->template("material")
            ->dataset('Current', $yearly_data->pluck('current'))
            ->dataset('Voltage', $yearly_data->pluck('voltage'))
            ->dataset('Power', $yearly_data->pluck('power'))            
            ->labels($yearly_data->pluck('datetime'));

            //yearly chart ends here
            



            //hourly chart starts here
            $current_time= Carbon::now(); 
            $subtracted_time = $current_time->subMinutes(60); 
            $current_time = $current_time->toTimeString();
            $subtracted_time = $subtracted_time->toTimeString();

            $hourly_data = Usage::where('motor_id',$motor_id)
                    ->where('time','<=',$current_time)
                    ->where('time','>=', $subtracted_time)
                    ->get();


            
           

            
                   

                    


            $hourly_chart = Charts::multi('line', 'highcharts')
            // Setup the chart settings
            ->title("Power,Voltage & Current Graph")
            // A dimension of 0 means it will take 100% of the space
            ->dimensions(0, 400)
             // Width x Height
           
            ->template("material")
            ->dataset('Current', $hourly_data->pluck('current'))
            ->dataset('Voltage', $hourly_data->pluck('voltage'))
            ->dataset('Power', $hourly_data->pluck('power'))            
            ->labels($hourly_data->pluck('datetime'));

            //hourlyly chart ends here
            

           
            
            //gauge1
            $voltage_gauge = Charts::realtime(route('randomData'), 1000, 'gauge', 'canvas-gauges')
            ->values([10, 0, 300])
            ->title(" ")
            ->elementLabel('Voltage')
            ->responsive(false)
            ->width(200);

            //gauge2
            $current_gauge = Charts::realtime(route('randomData'), 1000, 'gauge', 'canvas-gauges')
            ->values([10, 0, 10])
            ->title(" ")
            ->elementLabel('Current')
            ->responsive(false)
            ->width(200);

            //gauge3
            $power_gauge = Charts::realtime(route('randomData'), 1000, 'gauge', 'canvas-gauges')
            ->values([10, 0, 2000])
            ->title(" ")
            ->elementLabel('Power')
            ->responsive(false)
            ->width(200);

            
           
             return view('admin.pages.home',compact('usages','voltage_gauge','current_gauge','power_gauge','chart','daily_chart','weekly_chart','yearly_chart','hourly_chart'));
    }



    public function customdata(Request $request){
        
            $motor_id = Auth::user()->motor_id;
            $date=$request->all();
            $data=implode('', $date);
            list($from,$to)=explode("-", $data);
            
            $from=strtotime($from);
            $from = date('Y-m-d',$from);
            
            $to=strtotime($to);
            $to = date('Y-m-d',$to);

            
            $usages=Usage::where('motor_id',$motor_id)
                        ->whereBetween('date', [$from, $to])
                        //->orderBy('created_at', 'desc')
                        ->get();
            
                

            
            

            $chart = Charts::multi('line', 'highcharts')
            // Setup the chart settings
            ->title("Power,Voltage & Current Graph")
            // A dimension of 0 means it will take 100% of the space
            ->dimensions(0, 400) // Width x Height
            // This defines a preset of colors already done:)
            ->template("material")
            // You could always set them manually
            // ->colors(['#2196F3', '#F44336', '#FFC107'])
            // Setup the diferent datasets (this is a multi chart)
            ->dataset('Current', $usages->pluck('current'))
            ->dataset('Voltage', $usages->pluck('voltage'))
            ->dataset('Power', $usages->pluck('power'))            
            // Setup what the values mean
            ->labels($usages->pluck('created_at'));

        

            return view('admin.pages.customdata',compact('from','to','usages','chart'));

    }



    public function customPdf($from,$to){


            
            $motor_id = Auth::user()->motor_id;
                        
            $usages=Usage::where('motor_id',$motor_id)
                        ->whereBetween('date', [$from, $to])
                        ->get();
            
            $chart = Charts::multi('line', 'highcharts')
            // Setup the chart settings
            ->title("Power,Voltage & Current Graph")
            // A dimension of 0 means it will take 100% of the space
            ->dimensions(0, 400) // Width x Height
            // This defines a preset of colors already done:)
            ->template("material")
            // You could always set them manually
            // ->colors(['#2196F3', '#F44336', '#FFC107'])
            // Setup the diferent datasets (this is a multi chart)
            ->dataset('Current', $usages->pluck('current'))
            ->dataset('Voltage', $usages->pluck('voltage'))
            ->dataset('Power', $usages->pluck('power'))            
            // Setup what the values mean
            ->labels($usages->pluck('time'));

            //return view('admin.pages.customdataPdf',compact('usages','to','from','chart'));

            $pdf = PDF::loadView('admin.pages.customdataPdf',compact('usages','to','from','chart'));
            return $pdf->download('usages.pdf');
        
        

    }




}
