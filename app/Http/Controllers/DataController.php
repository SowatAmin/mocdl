<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usage;
class DataController extends Controller
{
    public function create(Request $request){
    	$data=new Usage;
    	$data=$request->input();
    	$data['power']=$data['current']*$data['voltage'];
    	$data['date']=date('Y-m-d');
    	$data['time']=date('H:i:s', time());
    	$data['datetime']=date('Y-m-d H:i:s');	
    	Usage::create($data);
        return "successfull";

    }
}
