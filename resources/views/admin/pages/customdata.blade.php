@extends('admin.layout')


@section('content')
<div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="x_panel">
                        <div class="x_title">
                           <h2>Tabulated Datas From <strong>{{$from}}</strong> To <strong>{{$to}}</strong></h2>
                           <ul class="nav navbar-right panel_toolbox">
                              <li><a href="/custom/pdf/{{$from}}/{{$to}}" style="color:red;"><i class="fa fa-download"></i><strong> Download as PDF</strong></a>
                              </li>

                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                           </ul>
                           <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                           <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                              
                              <div class="row">
                                 <div class="col-sm-12" id="app">
                                    <table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
                                       <thead>
                                          <tr role="row">
                                             <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"  style="width: 30px;">SL.</th>
                                             <th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" style="width: 100px;">Date (Y-M-D)</th>
                                             <th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" style="width: 120px;">Time</th>
                                             <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"  style="width: 160px;">Voltage (V)</th>
                                             <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"  style="width: 118px;">Current (I)</th>
                                             <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"  style="width: 113px;">Power (P)</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          @foreach($usages as $index => $usage)
                                          <tr role="row">
                                             <td>{{$index+1}}</td>
                                             <td>{{$usage->date}}</td>
                                             <td>{{$usage->time}}</td>
                                             <td>{{$usage->voltage}}</td>
                                             <td>{{$usage->current}}</td>
                                             <td>{{$usage->power}}</td>
                                          </tr>
                                          @endforeach
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="x_panel">
                        <div class="x_title">
                           <h2>Motor Usage Activities<small>Graph for Power,Voltage & Current</small></h2>
                           <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                           </ul>
                           <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                           <div class="panel-body">
                              {!! $chart->render() !!}
                           </div>
                        </div>
                     </div>
                  </div>
                  <br />                  
</div>

@endsection()

@section('essentialscripts')
<script>
   $(document).ready(function(){
    $('#datatable').DataTable();
   
   });
   
   
   $('#datatable').DataTable( {
        scrollY:        '50vh',
        scrollCollapse: true,
        paging:         false,
        "order": []
    } );
   
   setTimeout(function(){
   window.location.reload(1);
   }, 30000);
   
</script>
@endsection