@extends('admin.layout')


@section('content')
<div class="row">
  <div class="page-title">
              <div class="title_left">
                <h3 style="color:black;">Remote monitoring and data logging of a grid tie solar power plant</h3>
              </div>
  </div>
</div>

<br>

<div class="row">

                  <!-- <div class="col-md-3">
                     <div class="x_panel">
                        <div class="x_title">
                           <h2>Device Status</h2>
                           <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                           </ul>
                           <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                           <h4>Status</h4>
                           <div class="widget_summary">
                              <div class="w_center w_55">
                                 <div class="onoffswitch">
                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch">
                                    <label class="onoffswitch-label" for="myonoffswitch">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                    </label>
                                 </div>
                              </div>
                              
                              <div class="clearfix"></div>
                           </div>
                        </div>
                     </div>
                  </div> -->
                  <div class="col-md-4">
                     <div class="x_panel">
                        <div class="x_title">
                           <h2>Voltage</h2>
                           <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                           </ul>
                           <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                           <!-- <h4>Voltage</h4> -->
                           <div class="widget_summary">
                              <div class="w_center w_55">
                                 <!-- gauge goes here -->
                                 <center>{!! $voltage_gauge->render() !!}</center>
                                 
                              </div>
                              <div class="clearfix"></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="x_panel">
                        <div class="x_title">
                           <h2>Current</h2>
                           <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                           </ul>
                           <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                           <!-- <h4>Current</h4> -->
                           <div class="widget_summary">
                              <div class="w_center w_55">
                                 <!-- gauge goes here -->
                                 <center>{!! $current_gauge->render() !!}</center>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                        </div>
                     </div>
                  </div>
                   <div class="col-md-4">
                     <div class="x_panel">
                        <div class="x_title">
                           <h2>Power</h2>
                           <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                           </ul>
                           <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                           <!-- <h4>Power</h4> -->
                           <div class="widget_summary">
                              <div class="w_center w_55">
                                 <!-- gauge goes here -->
                                 <center>{!! $power_gauge->render() !!}</center>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                        </div>
                     </div>
                  </div>


                  <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="x_panel">
                        <div class="x_title">
                           <h2><i class="fa fa-table"></i> Tabulated Datas <small>Usage</small></h2>
                           <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                           </ul>
                           <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                           <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                              
                              <div class="row">
                                 <div class="col-sm-12" id="app">
                                    <table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
                                       <thead>
                                          <tr role="row">
                                             <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"  style="width: 30px;">SL.</th>
                                             <th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" style="width: 100px;">Date (Y-M-D)</th>
                                             <th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" style="width: 120px;">Time</th>
                                             <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"  style="width: 160px;">Voltage (V)</th>
                                             <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"  style="width: 118px;">Current (I)</th>
                                             <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"  style="width: 113px;">Power (P)</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          @foreach($usages as $index => $usage)
                                          <tr role="row">
                                             <td>{{$index+1}}</td>
                                             <td>{{$usage->date}}</td>
                                             <td>{{$usage->time}}</td>
                                             <td>{{$usage->voltage}}</td>
                                             <td>{{$usage->current}}</td>
                                             <td>{{$usage->power}}</td>
                                          </tr>
                                          @endforeach
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

					<!-- datepaicker form for selecting data -->
                   <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="x_panel">
                        <div class="x_title">
                           <h2><i class="fa fa-calendar"></i> Custom data picker <small>Dateicker</small></h2>
                           <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                           </ul>
                           <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                           <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                              
                              <div class="row">
                                 	<div class="col-sm-12" id="app">
										<!-- datepicker code goes here -->
										<div class="well" style="overflow: auto">
						                      <div class="col-md-4">
						                        Date Range Picker
						                        {!! Form::open(['url' => 'customdata','method' => 'get']) !!}

						                          <fieldset>
						                            <div class="control-group">
						                              <div class="controls">
						                                <div class="input-prepend input-group">
						                                  <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
						                                  <input type="text" style="width: 300px" name="dateValue" id="reservation" class="form-control" value="">
						                                  <button type="submit" class="btn btn-success">Submit</button>
						                                </div>
						                              </div>
						                            </div>
						                          </fieldset>
                                            
						                        {!! Form::close() !!}

						                      </div>
						                 </div>
                                 	</div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>


<!--                <div class="col-md-12 col-sm-12 col-xs-12">
 <div class="x_panel">
   <div class="x_title">
     <h2><i class="fa fa-line-chart"></i> Motor Usage Activities <small>Graph for Power,Voltage & Current</small></h2>
     <ul class="nav navbar-right panel_toolbox">
       <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
       </li>
     </ul>
     <div class="clearfix"></div>
   </div>
   <div class="x_content">


     <div class="" role="tabpanel" data-example-id="togglable-tabs">
       <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
         <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Daily</a>
         </li>
         <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Weekly</a>
         </li>
         <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Monthly</a>
         </li>
         <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Yearly</a>
         </li>

       </ul>
       <div id="myTabContent" class="tab-content">
         <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
           {!! $daily_chart->render() !!}
         </div>
         <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
            {!! $chart->render() !!}
         </div>
         <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
            {!! $chart->render() !!}
         </div>
         <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
            {!! $chart->render() !!}
         </div>
       </div>
     </div>

   </div>
 </div>
              </div> -->

                  

                <!-- Hourly Graph -->
              

               <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-line-chart"></i> Motor Usage Activities <small>Hourly Graph for Power,Voltage & Current</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  graph goes here
                   {!! $hourly_chart->render() !!}
                  
              
                  </div>
                </div>
              </div>



                  <!-- Daly Graph -->
              

               <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-line-chart"></i> Motor Usage Activities <small>Daily Graph for Power,Voltage & Current</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  graph goes here
                   {!! $daily_chart->render() !!}
                  
              
                  </div>
                </div>
              </div>


              <!-- Weekly Graph -->
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-line-chart"></i> Motor Usage Activities <small>Weekly Graph for Power,Voltage & Current</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <!-- graph goes here -->
                   {!! $weekly_chart->render() !!}
                  

                  </div>
                </div>
              </div>

              <!-- Monthly Graph -->
              <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-line-chart"></i> Motor Usage Activities <small>Monthly Graph for Power,Voltage & Current</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  graph goes here
                  //$chart->render()
                  </div>
                </div>
              </div> -->
              
              <!-- Yearly Graph -->
              <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-line-chart"></i> Motor Usage Activities <small>Yearly Graph for Power,Voltage & Current</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  graph goes here
                  // $yearly_chart->render()
                 
                  <div id="con"> </div>
              
                  </div>
                </div>
              </div> -->

                  
                  <br />
               </div>

@endsection()

@section('essentialscripts')
<script>
   $(document).ready(function(){
    $('#datatable').DataTable();
   
   });
   
   
   $('#datatable').DataTable( {
        scrollY:        '50vh',
        scrollCollapse: true,
        paging:         false,
        "order": []
    } );
   
  /* setTimeout(function(){
   window.location.reload(1);
   }, 30000);
*/



   
</script>
@endsection