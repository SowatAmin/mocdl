<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Report</title>
	{!! Charts::assets() !!}
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		body {
		}
		table{

		}
		th{
			text-align: left;
		}
		#verticalTable th{
			text-align: center;
			font-size: 15px;
			border: 1px solid black;
		}
		#verticalTable td{
			font-size: 14px;
			border: 1px dashed black;
		}
		#verticalTable{
			width: 100%;
			padding: 5px 0px 0px 0px;
			margin: 0px auto;
			border-collapse: collapse;
		}
		tr{
			margin-top: 10px;
		}
		/*#logo{
			width: 112px;
			height: 113px;
			padding-left:30px;
		}*/
	</style>
</head>

<body>
	<div class="page">

		<h3 align="center">
			MOCDL			
		</h3>
		<!-- <h4 align="center">
			Treatment Center for mental Illness & Drug Addiction<br>
			20/20 Tajmohal Road, Mohammadpur, Dhaka
		</h4> -->
		
		


		<table border=1 align="center" style="height:50px" >
			<tr>
				<td align="center"><strong>Datas from {{$from}} to {{$to}}</strong></td>
			</tr>
		</table>
		<br><br><br>

		<table width="100%" id="verticalTable">
			
			<tr>
				
				<th align="center">SL.</th>
                <th align="center">Date (Y-M-D)</th>
                <th align="center">Time</th>
                <th align="center">Voltage (V)</th>
                <th align="center">Current (I)</th>
                <th align="center">Power (P)</th>
				
				
			</tr>
			@foreach($usages as $index => $usage)
			<tr>
				
				<td align="center">{{$index+1}}</td>
                <td align="center">{{$usage->date}}</td>
                <td align="center">{{$usage->time}}</td>
                <td align="center">{{$usage->voltage}}</td>
                <td align="center">{{$usage->current}}</td>
                <td align="center">{{$usage->power}}</td>
						
			</tr>				
			@endforeach
			
		</table>

		
		

		


	</div>

</div>

</body>